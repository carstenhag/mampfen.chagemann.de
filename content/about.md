---
title: "Über mich"
date: 2021-01-13T17:15:31+01:00
---

Seit einigen Monaten bin ich Vegetarier und oft bin ich frustriert, weil es Lebensmittel nur mit Fleisch gibt. Und wenn ich welche finde, dann hab ich kein Plan ob diese gut schmecken oder nicht. Hier möchte ich es anderen erleichtern, gute und leckere Produkte ohne Fleisch zu finden!

Falls es beim Foto nicht anders dransteht, wurde es von mir selbst aufgenommen.


Impressum:  
Carsten Hagemann, Kirchstr. 1, 86511 Schmiechen  
carsten@chagemann.de

[Haupt-Blog über andere Dinge](https://chagemann.de)