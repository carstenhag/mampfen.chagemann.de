---
title: "dm Curry-Papaya-Mango-Aufstrich"
date: 2021-01-16T11:43:48+01:00
tags: [vegan, vegetarisch, aufstrich]
images: ["/images/aufstrich/dm-curry-papaya-mango-1.jpg"]
rating: 4
---

Fruchtiger Aufstrich, der nicht zu süß und nicht zu curry-lastig ist. Schmeckt mir und meiner Freundin gut. Muss man innerhalb von einer Woche verbrauchen, hält sich leider nicht allzu lange.


![Vorderseite der Verpackung und Aufstrich auf dem Brot](/images/aufstrich/dm-curry-papaya-mango-1.jpg)
![Rückseite der Verpackung 1](/images/aufstrich/dm-curry-papaya-mango-2.jpg)
![Rückseite der Verpackung 2](/images/aufstrich/dm-curry-papaya-mango-3.jpg)