---
title: "dm Kräuter-Pastete"
date: 2021-01-16T11:43:31+01:00
tags: [vegan, vegetarisch, aufstrich]
images: ["/images/aufstrich/dm-pastete-kraeuter-1.jpg"]
rating: 3
---

Sieht in der Dose und auf dem Brot sehr komisch aus. Ein hellbraunes Ungetüm, quasi. Schmecken tut es nach Leberwurst mit Schnittlauch, wobei ich gar nicht weiß ob es danach schmecken soll.

Komischerweise mag ich die Pastete manchmal, manchmal denke ich mir nach dem Essen aber auch, dass ich lieber was anderes hätte kaufen sollen.

![Vorderseite der Verpackung](/images/aufstrich/dm-pastete-kraeuter-1.jpg)
![Doseninhalt und Aufstrich auf dem Brot](/images/aufstrich/dm-pastete-kraeuter-2.jpg)
![Rückseite der Verpackung 1](/images/aufstrich/dm-pastete-kraeuter-3.jpg)
![Rückseite der Verpackung 2](/images/aufstrich/dm-pastete-kraeuter-4.jpg)