---
title: "dm Linseneintopf"
date: 2021-01-13T17:30:42+01:00
tags: [vegan, vegetarisch, fertiggericht]
images: ["/images/linsensuppe/dm-linsensuppe-1.jpg"]
rating: 5
---

Eine Suppe, die nach Linsen, Karotten und Kartoffeln schmeckt – so wie eine typische Linsensuppe schmecken muss! Nicht zu kartoffellastig und langweilig wie die von Erasco. Bisher die beste fleischlose Suppe für mich. Einziger Kritikpunkt ist die kleine Dose die nur 400g beinhaltet. Auch hierzu muss ich wieder etwas anderes Essen um ordentlich satt zu werden.


![Vorderseite der Dose](/images/linsensuppe/dm-linsensuppe-1.jpg)
![Rückseite der Dose](/images/linsensuppe/dm-linsensuppe-2.jpg)