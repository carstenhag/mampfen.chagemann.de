---
title: "Erasco Linsen-Eintopf"
date: 2020-11-10T19:27:48+01:00
tags: [vegan, vegetarisch, fertiggericht, suppe]
images: ["/images/linsensuppe/erasco-linseneintopf.jpg"]
rating: 3
---

Dieser "vegetarische" Linseneintopf ist eigentlich vegan (am Zeichen unten rechts erkennbar). Nicht nur da ist die Suppe etwas komisch, denn auch im Geschmack ist es eher eine Kartoffelsuppe und keine Linsensuppe. Ist aber die einzige fleischlose Linsensuppe beim Kaufland, was ich Schade finde.

![Vorderseite der Dose inkl. Preis](/images/linsensuppe/erasco-linseneintopf.jpg)
