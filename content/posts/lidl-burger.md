---
title: "Lidl Next Level Burger (bzw. Vemondo)"
date: 2020-11-17T19:28:12+01:00
tags: [vegan, vegetarisch, fertiggericht]
images: ["/images/burger/lidl-burger.jpg"]
rating: 3
---

Man verlangt bestimmt nicht viel von einem Mikrowellen-Burger, und hier bekommt man genau so wenig, wie man erwartet hatte. Zwei latschrige Bürgerbrötchen mit einem ähnlich tollem Burger-Patty. Am besten also weiterhin selbst die Brötchen belegen.

Nachtrag: Lidl's fleischlos-Marke ist nun "Vemondo", die Produkte sind aber wahrscheinlich noch die gleichen.

![Vorderseite der Verpackung](/images/burger/lidl-burger.jpg)