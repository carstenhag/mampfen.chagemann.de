---
title: "Lidl Next Level Nuggets (bzw. Vemondo)"
date: 2021-01-14T21:51:09+01:00
tags: [vegan, vegetarisch, fertiggericht]
images: ["/images/fertiggericht/lidl-nuggets-1.jpg"]
rating: 4
---

Als ich noch Fleisch aß, hatte ich mir beim McDonald'ss üblicherweise McNuggets geholt. Die haben Stand 2021 aber noch keine fleischlosen, deswegen wollte ich mal die vom Lidl probieren. Da meine Freundin und ich leider unsere Fritteuse gegen die Wand umgekippt haben und sie seitdem Schrott ist (die Fritteuse), musste ich die Nuggets im Ofen zubereiten.

Nach 20 Minuten waren die Nuggets schön knusprig. Die Sweet-Chili-Sauce hab' ich wie auf der Packung empfohlen per Wasserbad aufgetaut. Komischerweise haben die Nuggets etwas fad geschmeckt. Irgendein Gewürz fehlt! Wenn ich noch ein bisschen rumprobiert hätte, hätt' ich bestimmt das Richtige gefunden. Vielleicht hat einfach nur Salz gefehlt?

Die Sauce war 95% Sweet und nur 5% Chili. Die Süß- und Sauer-Sauce vom McDonald's mag ich viel lieber. Wenn man das also als Fertiggericht/Snack zusammen ist, ist's nicht so berauschend. Ich kann mir die Nuggets aber sehr gut als Beilage zu einem anderen Essen vorstellen, um eine zusätzliche, fleischartige Textur zu erhalten.

Nachtrag: Lidl's fleischlos-Marke ist nun "Vemondo", die Produkte sind aber wahrscheinlich noch die gleichen.

![Vorderseite der Verpackung](/images/fertiggericht/lidl-nuggets-1.jpg)

Zubereitet im Ofen:
![Nuggets mit Dip](/images/fertiggericht/lidl-nuggets-2.jpg)
![Rückseite der Verpackung](/images/fertiggericht/lidl-nuggets-3.jpg)