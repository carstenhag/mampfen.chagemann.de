---
title: "Rewe Chili Sin Carne"
date: 2021-01-07T19:28:21+01:00
tags: [vegan, vegetarisch, fertiggericht, suppe]
images: ["/images/chili-sin-carne/rewe-chili-sin-carne.jpg"]
rating: 4
---

Da ich mich nicht nur von Linsensuppen ernähren kann und es beim Kaufland/Lidl keine fleischlosen Suppen gibt, wurde ich beim Rewe fündig. Leider waren mir die Bohnen etwas zu mehlig und es wirkte so, als würde irgendetwas fehlen. Trotzdem war es lecker, werde ich also wieder kaufen!

Mit 400g bzw. 350 kCal, ähnlich schlecht sättigend wie andere Suppen. Warum müssen vegane Suppen immer in kleinen Dosen verkauft werden? 

![Vorderseite der Dose](/images/chili-sin-carne/rewe-chili-sin-carne.jpg)