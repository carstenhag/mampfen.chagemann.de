---
title: "Rewe Bio Linseneintopf"
date: 2021-04-25T10:09:56+02:00
tags: [vegan, vegetarisch, fertiggericht]
images: ["/images/linsensuppe/rewe-bio-linseneintopf-1.jpg"]
rating: 5
---

Geschmacklich so gut wie die [Suppe von DM](/posts/dm-linsensuppe/), vielleicht sogar ein Ticken besser. Leider ebenfalls nur in einer Mini-Dose verfügbar.

Laut Label ist die Suppe nur vegetarisch, aber laut der Zutatenliste beinhaltet es keine tierischen Produkte. Da muss Rewe noch nachbessern damit es klarer wird :/. 


![Vorderseite der Verpackung](/images/linsensuppe/rewe-bio-linseneintopf-1.jpg)
