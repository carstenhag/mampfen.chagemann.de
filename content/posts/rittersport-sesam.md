---
title: "Rittersport Sesam"
date: 2021-05-20T10:09:56+02:00
tags: [vegan, vegetarisch, schokolade]
images: ["/images/schokolade/rittersport-sesam-1.jpg"]
rating: 5
---

Rittersport hat 3 explizit vegane Schokoladentafeln rausgebracht. Neben "Voll-Nuss Amaranth" und "Mandel Quinoa" gibt es noch diese hier, die "Sesam"-Tafel.

Diese hier gefällt mir am meisten, sie schmeckt sehr cremig und durch das Sesam hat man noch eine andere Geschmackstextur dabei. Die beste vegane Schokolade für mich, die nicht nach Zartbitter schmeckt!

Da die Schokolade auch etwas knusprig ist, und ich bisher kein veganes Müsli-Mix gefunden hatte das ich mochte, hab ich 2-3 Stücke mit einem Stabmixer-Zerkleinerer quasi püriert und mixe dies nun in das Müsli rein.

![Vorderseite der Verpackung](/images/schokolade/rittersport-sesam-1.jpg)

![Rückseite der Verpackung mit Inhalt](/images/schokolade/rittersport-sesam-2.jpg)