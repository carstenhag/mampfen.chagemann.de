---
title: "Tartex Linsen Curry-Aufstrich"
date: 2020-12-28T19:28:05+01:00
tags: [vegan, vegetarisch, aufstrich]
images: ["/images/aufstrich/tartex-gelbe-linsen-curry.jpg"]
rating: 5
---

Ein richtig leckerer Curry-Aufstrich! Weder zu cremig, noch zu Curry-lastig. Genau richtig abgestimmt. Gibt's beim dm, leider habe ichs noch nicht bei anderen Läden gesehen.

![Vorderseite der Verpackung](/images/aufstrich/tartex-gelbe-linsen-curry.jpg)