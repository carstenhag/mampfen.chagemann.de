---
title: "Vantastic Foods Hefeflocken"
date: 2021-06-03T10:09:56+02:00
tags: [vegan, vegetarisch]
images: ["/images/kaese/vantastic-hefeflocken-1.jpg"]
rating: 3
---

Mit Hefeflocken kann man etwas vergleichbares zu geschmolzenem Käse zubereiten. Dafür gibt es verschiedene Rezepte, ich habe aber bisher nur eins probiert und war noch nicht zu 100% überzeugt. Auf einer Pizza hatte es solala bis gut geschmeckt. Werde es das nächste Mal mit einem anderen Rezept ausprobieren.

Wo wir gerade bei Rezepten sind: Ich finde schade, dass auf der Verpackung keine Zubereitungsempfehlung steht, das würde das ganze einfacher machen.

Die Hefeflocken habe ich vor Ort bei einem Rewe gefunden, es gibt sie aber auch bei [Vantastic Foods direkt](https://www.vantastic-foods.com/vegane-lebensmittel/grundausstattung/gewuerze-und-wuerzmittel/vantastic-foods-hefeflocken-200g).


![Vorderseite der Verpackung](/images/kaese/vantastic-hefeflocken-1.jpg)
![Rückseite der Verpackung](/images/kaese/vantastic-hefeflocken-2.jpg)